var __airGMS = __airGMS || {};

__airGMS.Validator = (function() {
    var errors = {};

    return {
        /**
         * Валидирует данные
         *
         * @param phone
         * @param message
         *
         * @returns boolean
         */
        validate: function(phone, message) {
            // чистим ошибки
            this.cleanErrors();

            if (phone == '') {
                this.addError('phone', 'Phone cannot be blank.');
            }

            if (phone.match(/^\+\d+$/g) == null) {
                this.addError('phone', 'Phone must be as integer and start with "+" char, example: +79507403941');
            }

            if (phone.length > 16 ) {
                this.addError('phone', 'Phone must be less 15 char');
            }

            if (phone.length < 12 ) {
                this.addError('phone', 'Phone must be more 10 char');
            }

            if (message == '') {
                this.addError('message', 'Message cannot be blank.');
            }

            if (message.length > 140 ) {
                this.addError('message', 'Message must be less 140 char');
            }

            return this.hasErrors();
        },

        /**
         * Добавляет ошибку
         *
         * @param attribute
         * @param error
         */
        addError: function (attribute, error) {
            errors[attribute].push(error);
        },

        /**
         *
         * @returns {boolean}
         */
        hasErrors: function () {
            var errors = this.getErrors();

            for(var error in errors) {
                if (errors[error].length > 0) {
                    return false;
                }
            }

            return true;
        },

        /**
         * Возвращает ошибки
         *
         * @returns {{}}
         */
        getErrors: function () {
            return errors;
        },

        /**
         * Очищаем ошибки
         */
        cleanErrors: function () {
            errors = {
                phone: [],
                message: []
            };
        }
    };
})();

