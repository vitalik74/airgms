__airGMS = (function () {
    // валидатор, шаблонизатор
    var validator = Object.create(__airGMS.Validator);
    var template = Object.create(__airGMS.Template);

    //
    var $form = $('#send-button'),
        $progress = $('.progress'),
        $template = $('#template').html(),
        $firstMessageSelector = $('#first-message'),
        $phone = $('#phone'),
        $message = $('#message'),
        hideClass = 'hide',
        errorClass = 'has-error',
        successClass = 'has-success',
        helpClass = 'help-block',
        url = '/send.php',
        initData = __airGMSData;


    return {
        /**
         * Вешаем событие на добавление формы
         */
        addFormListener: function () {
            var self = this;

            $form.click(function (e) {
                if (self.validateForm($phone.val(), $message.val())) {
                    // помечаем валидными
                    self.mark($phone);
                    self.mark($message);

                    // показываем прогресс
                    $progress.toggleClass(hideClass);

                    $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                phone: $phone.val(),
                                message: $message.val()
                            },
                            success: function (response) {
                                $progress.toggleClass(hideClass);

                                if (response.success == true) {
                                    $firstMessageSelector.after(template.interpolate($template, response.response));
                                    $message.val('');
                                } else {
                                    self.showValidateErrors(response.errors);
                                }

                            },
                            dataType: 'json'
                        }
                    );
                } else {
                    self.showValidateErrors();
                }

                return false;
            });
        },

        /**
         * Валидация
         *
         * @param phone
         * @param message
         * @returns {*|boolean}
         */
        validateForm: function (phone, message) {
            return validator.validate(phone, message);
        },

        showValidateErrors: function (errors) {
            if (errors == undefined) {
                var errors = validator.getErrors();
            }

            var errorsValues = {},
                self = this;

            if (errors.phone != undefined) {
                errorsValues.$phone = errors.phone.join(', ');
            }

            if (errors.message != undefined) {
                errorsValues.$message = errors.message.join(', ');
            }

            $.each(errorsValues, function (index, error) {
                var object = eval(index);

                if (error != '') {
                    self.mark(object, error, 'error');
                } else {
                    self.mark(object);
                }
            });
        },

        /**
         * Помечаем поля ошибкой или наоборот
         *
         * @param type
         * @param object
         * @param error
         */
        mark: function (object, error, type) {
            if (type == 'error') {
                object.parent().find('.' + helpClass).html(error);
                object.parent().addClass(errorClass);
            } else {
                object.parent().find('.' + helpClass).html('');
                object.parent().removeClass(errorClass).addClass(successClass);
            }
        },

        /**
         * Выводим сообщения сохраненные
         */
        initRenderMessages: function () {
            $phone.val(initData.lastPhone);

            $.each(this.sort(initData.history), function (index, response) {
                $firstMessageSelector.after(template.interpolate($template, response));
            });
        },

        /**
         * Сортируем массив относительно timestamp
         * @param array
         * @returns {*}
         */
        sort: function (array) {
            if (Array.isArray(array)) {
                array.sort(function (a, b) {
                    if (a.time > b.time) {
                        return 1;
                    }
                    if (a.time < b.time) {
                        return -1;
                    }

                    return 0;
                });
            }

            return array;
        },

        /**
         * Инициализация
         */
        init: function () {
            this.validateForm($phone.val(), $message.val());

            // инициализируем сообщения, которые уже были
            this.initRenderMessages();

            // вешаем события на сохрание формы
            this.addFormListener();
        }
    };
}).call(__airGMS || {});
