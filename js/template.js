var __airGMS = __airGMS || {};

__airGMS.Template = (function() {
    return {
        /**
         * Интерполирует название переменных в их значения
         *
         * @param html
         * @param attributes
         * @returns {*}
         */
        interpolate: function(html, attributes) {
            var key;

            for (key in attributes) {
                html = html.replace(new RegExp('\\{{' + key + '\\}}', 'gm'), attributes[key]);
            }

            return html;
        }
    };
})();
