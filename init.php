<?php
use app\components\model\Sms;
use app\components\repository\JsonRepository;

$loader = include_once 'vendor/autoload.php';

$repository = new JsonRepository();
$data = $repository->get();
$result = [
    'history' => '',
    'lastPhone' => ''
];

/** @var Sms $item */
foreach ($data as $item) {
    $result['history'][] = [
        'message' => $item->message,
        'time' => $item->time,
        'timestamp' => $item->getTimestamp(),
        'phone' => $item->phone
    ];
}

/** @var Sms $endSms */
$endSms = end($data);

if (!empty($endSms)) {
    $result['lastPhone'] = $endSms->phone;
}

return json_encode($result);

