<?php

namespace app\components\model;

class Sms
{
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $message;
    /**
     * @var string
     */
    public $time;


    /**
     * Sms constructor.
     *
     * @param $phone
     * @param $message
     * @param $time
     */
    public function __construct($phone, $message, $time)
    {
        $this->phone = $phone;
        $this->message = $message;
        $this->time = $time;
    }

    /**
     * @return false|string
     */
    public function getTimestamp()
    {
        return date('d.m.Y h:i:s', $this->time);
    }
}
