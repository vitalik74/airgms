<?php

namespace app\components\validator;

use app\components\error\ErrorInterface;

class SmsValidator implements ValidatorInterface, ErrorInterface
{
    private $_errors = [
        'phone' => [],
        'message' => []
    ];
    /**
     * Проверяем:
     *
     * - чтобы смс не было больше 140 символов (вроде столько помещается в смс)
     * - в номере, чтобы были все цифры и начиналось с +
     * - длина телефона(самих цифр) не должна быть больше 15 цифр (https://ru.wikipedia.org/wiki/E.164)
     * и не меньше 11
     * - обязательные поля телефон и сообщение
     *
     *
     * @param $attributes
     *
     * @return bool
     */
    public function validate($attributes)
    {
        $phone = $attributes['phone'];
        $message = $attributes['message'];

        if (empty($phone)) {
            $this->addError('phone', 'Phone cannot be blank.');
        }

        if (!preg_match('/^\+\d+$/is', $phone)) {
            $this->addError('phone', 'Phone must be as integer and start with "+" char, example: +79507403941');
        }

        if (strlen($phone) > 16) {
            $this->addError('phone', 'Phone must be less 15 char');
        }

        if (strlen($phone) < 12) {
            $this->addError('phone', 'Phone must be more 10 char');
        }

        if (empty($message)) {
            $this->addError('message', 'Message cannot be blank.');
        }

        if (mb_strlen($message, 'UTF8') > 140) {
            $this->addError('message', 'Message must be less 140 char');
        }

        return !$this->hasErrors();
    }

    /**
     * @param $attribute
     * @param $error
     *
     */
    public function addError($attribute, $error)
    {
        $this->_errors[$attribute][] = $error;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        if (!empty($this->_errors['phone']) || !empty($this->_errors['message'])) {
            return true;
        }

        return false;
    }
}
