<?php
namespace app\components\validator;


interface ValidatorInterface {
    /**
     * @param $attributes
     *
     * @return bool
     */
    public function validate($attributes);
}
