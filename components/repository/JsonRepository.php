<?php

namespace app\components\repository;

use app\components\model\Sms;

class JsonRepository implements JsonRepositoryInterface
{
    /**
     * @var \Directory
     */
    protected $dir = '/../../data';
    /**
     * @var string
     */
    protected $fileName = 'data.json';


    /**
     * SmsRepository constructor.
     */
    public function __construct()
    {
        $this->dir = __DIR__ . $this->dir;

        $this->checkDir();
        $this->checkFile();
    }

    /**
     * Блокируем, читаем, а потом записываем.
     *
     * Можно было сначала прочитать методом JsonRepository::get(), а потом записать,
     * в плане чистоты кода было бы лучше, но в промежуток между прочтением и записью потом может
     * другой клиент захватить и что-то записать. И в нашем прочтении не будет данной записи,
     * и мы потом свое запишем. То есть вклиненную запись потеряем.
     *
     *
     * @param Sms $sms
     *
     * @return bool
     *
     */
    public function save(Sms $sms)
    {
        $fo = fopen($this->generateFileName(), "r");

        try {
            if (flock($fo, LOCK_EX)) {
                $data = fread($fo, filesize($this->generateFileName()));
                fclose($fo);

                $fo = fopen($this->generateFileName(), "w");
                $data = json_decode($data, true);

                $data[] = [// маленькие переменные, экономим место на диске
                    'm' => $sms->message,
                    't' => $sms->time,
                    'p' => $sms->phone
                ];

                $resultPut = fputs($fo, json_encode($data));
                $resultLock = flock($fo, LOCK_UN);

                if (!empty($resultPut) && !empty($resultLock)) {
                    return true;
                } elseif (empty($resultLock)) {
                    flock($fo, LOCK_UN);
                }
            }

            fclose($fo);
        } catch (\Exception $e) { // а вдруг json битый, а мы заблочили
            flock($fo, LOCK_UN);
        }

        return false;
    }

    /**
     * @return array
     */
    public function get()
    {
        $fo = fopen($this->generateFileName(), "r");
        $result = [];

        try {
            if (flock($fo, LOCK_SH)) {
                $data = fread($fo, filesize($this->generateFileName()));
                $data = json_decode($data);

                foreach ($data as $item) {
                    $result[] = new Sms($item->p, $item->m, $item->t);
                }

                $resultLock = flock($fo, LOCK_UN);

                if (!empty($resultLock)) {
                    return $result;
                } elseif (empty($resultLock)) {
                    flock($fo, LOCK_UN);
                }
            }

            fclose($fo);
        } catch (\Exception $e) { // а вдруг json битый, а мы заблочили
            flock($fo, LOCK_UN);
        }

        return $result;
    }

    /**
     *
     */
    protected function checkDir()
    {
        if (!is_dir($this->dir)) {
            mkdir($this->dir, '0777');
        }
    }

    /**
     *
     */
    protected function checkFile()
    {
        $fileName = $this->generateFileName();

        if (!is_file($fileName) && !file_exists($fileName)) {
            $fo = fopen($this->generateFileName(), "w+");

            try {
                if (flock($fo, LOCK_EX)) {
                    $data = [];

                    $resultPut = fputs($fo, json_encode($data));
                    $resultLock = flock($fo, LOCK_UN);

                    if (!empty($resultPut) && !empty($resultLock)) {
                        return true;
                    } elseif (empty($resultLock)) {
                        flock($fo, LOCK_UN);
                    }
                }

                fclose($fo);
            } catch (\Exception $e) { // а вдруг json битый, а мы заблочили
                flock($fo, LOCK_UN);
            }
        }
    }

    /**
     * @return string
     */
    protected function generateFileName()
    {
        return $this->dir . DIRECTORY_SEPARATOR .$this->fileName;
    }
}
