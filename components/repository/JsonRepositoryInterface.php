<?php

namespace app\components\repository;

use app\components\model\Sms;

interface JsonRepositoryInterface
{
    /**
     * @param Sms $sms
     *
     * @return bool
     */
    public function save(Sms $sms);

    /**
     * @return []
     */
    public function get();
}
