<?php

namespace app\components\provider\sms;

/**
 * Interface SmsInterface
 *
 * @package app\provider\sms
 */
interface SmsProviderInterface {
    /**
     * @param $phone
     * @param $message
     *
     * @return mixed
     */
    public function send($phone, $message);
}
