<?php

namespace app\components\provider\sms;

use app\components\error\ErrorInterface;
use Plivo\RestAPI;

class PlivoSmsProvider implements SmsProviderInterface, ErrorInterface
{
    /**
     * @var RestAPI
     */
    private $_provider;
    /**
     * @var
     */
    private $_errors;


    /**
     * PlivoSmsProviderProvider constructor.
     *
     * @param string $authId
     * @param string $authToken
     */
    public function __construct($authId = 'SAYTG2MGIXYJK1ZJA2OW', $authToken = 'MDc2MDJjZjlmZTUxZDFiNmY0ZmFhNjhhNzlkNmY5')
    {
        $this->_provider = new RestAPI($authId, $authToken);
    }


    /**
     * @param $phone
     * @param $message
     *
     * @return mixed
     */
    public function send($phone, $message)
    {
        // Use TextID to send to Russia, or full number of sender (+17787290797) to send to US/Canada
        if (strpos($phone, '+7') == 0) {
            $src = 'AirGMS';
        } else {
            $src = '+17787290797';
        }
        // если хотите проверить на получение ошибки из смс сервиса, то раскомментируйте ниже телефон
        //$phone = 'sdfsdfsdf';
        $params = [
            'src' => $src,
            'dst' => $phone,
            'text' => $message,
            'type' => 'sms'
        ];
        $response = $this->_provider->send_message($params);

        if ($response['status'] == 202) {
            return true;
        }

        $this->addError('', $response['response']['error']);

        return false;
    }

    /**
     * @return []
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @param $attribute
     * @param $error
     *
     */
    public function addError($attribute, $error)
    {
        $this->_errors['phone'][] = $error;
    }

    /**
     * @return boolean
     */
    public function hasErrors()
    {
        return count($this->_errors);
    }
}
