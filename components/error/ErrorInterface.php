<?php

namespace app\components\error;

interface ErrorInterface
{
    /**
     * @return []
     */
    public function getErrors();

    /**
     * @param $attribute
     * @param $error
     *
     */
    public function addError($attribute, $error);

    /**
     * @return boolean
     */
    public function hasErrors();
}
