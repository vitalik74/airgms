<?php

namespace app\components\handler;

use app\components\error\ErrorInterface;
use app\components\model\Sms;
use app\components\provider\sms\SmsProviderInterface;
use app\components\repository\JsonRepository;
use app\components\validator\ValidatorInterface;
use Exception;

class SmsHandler
{
    /**
     * @var Sms
     */
    private $_model;
    /**
     * @var ValidatorInterface|ErrorInterface
     */
    private $_validator;
    /**
     * @var JsonRepository
     */
    private $_repository;
    /**
     * @var SmsProviderInterface|ErrorInterface
     */
    private $_smsProvider;


    public function __construct(Sms $model, ValidatorInterface $validator, JsonRepository $repository, SmsProviderInterface $smsProvider)
    {
        $this->_model = $model;
        $this->_validator = $validator;
        $this->_repository = $repository;
        $this->_smsProvider = $smsProvider;
    }

    /**
     * @return array
     */
    public function handle()
    {
        $validate = $this->_validator->validate([
            'phone' => $this->_model->phone,
            'message' => $this->_model->message
        ]);

        if ($validate) {
            try {
                $response = $this->_smsProvider->send($this->_model->phone, $this->_model->message);

                if ($response) {
                    if ($this->_repository->save($this->_model)) {
                        return [
                            'success' => true,
                            'response' => [
                                'message' => $this->_model->message,
                                'phone' => $this->_model->phone,
                                'timestamp' => $this->_model->getTimestamp()
                            ]
                        ];
                    } else {
                        return [
                            'success' => false,
                            'errors' => [
                                'phone' => ['Not save in history']
                            ]
                        ];
                    }
                } else {
                    return [
                        'success' => false,
                        'errors' => $this->_smsProvider->getErrors()
                    ];
                }
            } catch (Exception $e) {
                return [
                    'success' => false,
                    'errors' => [
                        'phone' => ['System error'],
                    ]
                ];
            }
        } else {
            return [
                'success' => false,
                'errors' => $this->_validator->getErrors()
            ];
        }
    }
}
