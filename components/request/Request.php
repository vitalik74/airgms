<?php

namespace app\components\request;

class Request
{
    /**
     * @param $attribute
     * @param string $default
     *
     * @return mixed
     */
    public function post($attribute, $default = '')
    {
        return $this->getParams($attribute, $_POST, $default);
    }

    /**
     * @param $attribute
     * @param string $default
     *
     * @return mixed
     */
    public function get($attribute, $default = '')
    {
        return $this->getParams($attribute, $_GET, $default);
    }

    /**
     * @param $attribute
     * @param array $from
     * @param $default
     *
     * @return mixed
     */
    protected function getParams($attribute, array $from, $default)
    {
        return isset($from[$attribute]) ? $from[$attribute] : $default;
    }
}
