<?php

use app\components\handler\SmsHandler;
use app\components\model\Sms;
use app\components\provider\sms\PlivoSmsProvider;
use app\components\repository\JsonRepository;
use app\components\request\Request;
use app\components\validator\SmsValidator;

$loader = include_once 'vendor/autoload.php';

$request = new Request();
$model = new Sms($request->post('phone'), $request->post('message'), time());
$repository = new JsonRepository();
$validator = new SmsValidator();
$smsProvider = new PlivoSmsProvider();

$handler = new SmsHandler($model, $validator, $repository, $smsProvider);

$result = $handler->handle();

echo json_encode($result);
