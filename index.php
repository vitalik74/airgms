<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>AirGMS</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">

    <div class="starter-template">
        <h1>Hello, AirGMS!</h1>

        <div class="row">

            <div class="col-md-12">
                <div class="progress hide">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50"
                         aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                        <span class="sr-only">50% Complete</span>
                    </div>
                </div>

<!--                <form id="form">-->
                    <div class="form-group">
                        <label for="email">Recipient:</label>
                        <input type="text" class="form-control" id="phone"
                               placeholder="Phone number of recipient" maxlength="16">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label for="text">Text:</label>
                        <input type="text" class="form-control" id="message" placeholder="Text" maxlength="140">
                        <span class="help-block"></span>
                    </div>

                    <button type="submit" class="btn btn-default" id="send-button">Send</button>
<!--                </form>-->

                <br><br>
            </div>

            <div id="first-message"></div>

        </div>

    </div>

</div>

<script id="template" type="text/airgms-tmpl">
    <div class="col-md-12 message">
        <div class="alert alert-success" role="alert">
            <h4>{{message}}</h4>
            {{phone}}
            <p class="small">{{timestamp}}</p>
        </div>
    </div>
</script>

<script>
        var __airGMSData = <?= include 'init.php'?>;
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/vendor/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/vendor/bootstrap.min.js"></script>
<script src="/js/validator.js"></script>
<script src="/js/template.js"></script>
<script src="/js/application.js"></script>
<script src="/js/common.js"></script>
</body>
</html>